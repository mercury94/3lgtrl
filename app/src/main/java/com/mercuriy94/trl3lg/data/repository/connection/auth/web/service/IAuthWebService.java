package com.mercuriy94.trl3lg.data.repository.connection.auth.web.service;

import com.mercuriy94.trl3lg.data.Entity.rest.request.IsValidSessionTokenRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.LoginRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.AuthResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.IsValidSessionTokenResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by nikita on 02.06.17.
 */

public interface IAuthWebService {

    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<AuthResponse> login(@Body LoginRequest authRequest);

    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<IsValidSessionTokenResponse> isValidSessionToken(@Body IsValidSessionTokenRequest request);
}
