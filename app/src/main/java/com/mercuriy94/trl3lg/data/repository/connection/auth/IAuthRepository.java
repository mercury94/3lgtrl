package com.mercuriy94.trl3lg.data.repository.connection.auth;

import com.mercuriy94.trl3lg.data.Entity.rest.request.BaseRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.IsValidSessionTokenRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.LoginRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.AuthResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.IsValidSessionTokenResponse;

import java.util.Set;

import io.reactivex.Observable;

/**
 * Created by nikita on 02.06.17.
 */

public interface IAuthRepository {

    Observable<AuthResponse> auth(LoginRequest authRequest);

    Observable<Boolean> saveSessionToken(String sessionToken);

    Observable<Boolean> saveModuleId(String moduleId);

    Observable<String> getSessionToken();

    Observable<String> getModuleId();

    Observable<Boolean> saveRentalModuleIds(Set<String> moduleIds);

    Observable<Set<String>> getRentalModuleIds();

    Observable<IsValidSessionTokenResponse> isValidSessionToken(IsValidSessionTokenRequest request);
}
