package com.mercuriy94.trl3lg.data.repository.connection.signup.web.service;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetFieldsForSignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.SignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetFieldsForSignUpResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.SignUpResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by nikita on 12.06.17.
 */

public interface ISignUpWebService {


    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<GetFieldsForSignUpResponse> getFieldsForSignUp(@Body GetFieldsForSignUpRequest getFieldsForSignUpRequest);


    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<SignUpResponse> signUp(@Body SignUpRequest signUpRequest);


}
