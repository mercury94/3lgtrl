package com.mercuriy94.trl3lg.data.repository.connection.site;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetSitesRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetSitesResponse;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;

/**
 * Created by nikita on 03.06.17.
 */

public interface ISiteRepository {

    Observable<GetSitesResponse> getSites(GetSitesRequest getSitesRequest);

    Observable<Boolean> saveSiteId(String siteId);

    Observable<String> getSiteId();

}
