package com.mercuriy94.trl3lg.data.Entity.rest.response;

/**
 * Created by nikita on 26.07.17.
 */

public class IsValidSessionTokenResponse extends BaseResponse<Integer> {
    public static final int SESSION_VALID = 1;
    public static final int SESSION_NOT_VALID = 0;
}
