package com.mercuriy94.trl3lg.data.repository.connection.auth.web;

import com.mercuriy94.trl3lg.data.Entity.rest.request.IsValidSessionTokenRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.LoginRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.AuthResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.IsValidSessionTokenResponse;
import com.mercuriy94.trl3lg.data.repository.connection.auth.web.service.IAuthWebService;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by nikita on 02.06.17.
 */

public class AuthWebRepository implements IAuthWebRepository {

    @Inject
    protected IAuthWebService mAuthService;

    @Inject
    public AuthWebRepository() {
    }

    @Override
    public Observable<AuthResponse> auth(LoginRequest authRequest) {
        return mAuthService.login(authRequest);
    }

    @Override
    public Observable<IsValidSessionTokenResponse> isValidSessionToken(IsValidSessionTokenRequest request) {
        return mAuthService.isValidSessionToken(request);
    }
}
