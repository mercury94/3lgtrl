package com.mercuriy94.trl3lg.data.Entity;

/**
 * Created by Nikita on 01.05.2017.
 */

public class ConnectionSettings {

    private String mAddress;
    private String mPort;


    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        mPort = port;
    }

}
