package com.mercuriy94.trl3lg.data.repository.connection.rental.book.web.service;

import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchBookListRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchQueueRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchRentalGroupsRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchBookListResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchQueueBooksResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchRentalGroupsResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by mercuriy94 on 27.07.17.
 */

public interface IRentalBookWebService {

    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<List<FetchRentalGroupsResponse>> getGroups(@Body List<FetchRentalGroupsRequest> request);

    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<List<FetchBookListResponse>> fetchBooks(@Body List<FetchBookListRequest> requests);

    @Headers("Content-Type: application/json")
    @POST("core/webservice")
    Observable<List<FetchQueueBooksResponse>> fetchQueue(@Body List<FetchQueueRequest> requests);


}
