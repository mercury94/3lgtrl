package com.mercuriy94.trl3lg.data.Entity.rest.response;

import com.mercuriy94.trl3lg.data.Entity.RentalGroup;
import com.mercuriy94.trl3lg.data.Entity.dto.RentalGroupDto;

import java.util.List;

/**
 * Created by nikita on 26.07.17.
 */

public class FetchRentalGroupsResponse extends BaseResponse<List<RentalGroupDto>> {


}
