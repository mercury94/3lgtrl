package com.mercuriy94.trl3lg.data.repository.connection.rental.book;

import com.mercuriy94.trl3lg.data.Entity.AudioBook;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchBookListRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchRentalGroupsRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchBookListResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchRentalGroupsResponse;
import com.mercuriy94.trl3lg.data.repository.connection.rental.book.local.IRentalBookLocalRepository;
import com.mercuriy94.trl3lg.data.repository.connection.rental.book.web.IRentalBookWebRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by nikita on 26.07.17.
 */

public class RentalBookRepository implements IRentalBookRepository {

    @Inject
    IRentalBookWebRepository mRentalBookWebRepository;

    @Inject
    IRentalBookLocalRepository mRentalBookLocalRepository;

    @Inject
    public RentalBookRepository() {
    }

    @Override
    public Observable<List<FetchRentalGroupsResponse>> getGroups(List<FetchRentalGroupsRequest> request) {
        return mRentalBookWebRepository.getGroups(request);
    }

    @Override
    public Observable<List<FetchBookListResponse>> fetchBooks(List<FetchBookListRequest> requests) {
        return mRentalBookWebRepository.fetchBooks(requests);
    }

    @Override
    public Observable<List<AudioBook>> fetchMyAudioBooks() {
        return mRentalBookWebRepository.fetchMyAudioBooks();
    }
}
