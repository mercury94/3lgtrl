package com.mercuriy94.trl3lg.data.repository.connection.signup;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetFieldsForSignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.SignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetFieldsForSignUpResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.SignUpResponse;
import com.mercuriy94.trl3lg.data.repository.connection.signup.web.ISignUpWebRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by nikita on 12.06.17.
 */

public class SignUpRepository implements ISignUpRepository {

    @Inject
    protected ISignUpWebRepository mSignUpWebRepository;

    @Inject
    public SignUpRepository() {
    }

    @Override
    public Observable<GetFieldsForSignUpResponse> getFieldsForSignUp(GetFieldsForSignUpRequest getFieldsForSignUpRequest) {
        return mSignUpWebRepository.getFieldsForSignUp(getFieldsForSignUpRequest);
    }

    @Override
    public Observable<SignUpResponse> signUp(SignUpRequest signUpRequest) {
        return mSignUpWebRepository.signUp(signUpRequest);
    }
}
