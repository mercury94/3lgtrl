package com.mercuriy94.trl3lg.data.repository.connection.signup.web;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetFieldsForSignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.SignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetFieldsForSignUpResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.SignUpResponse;
import com.mercuriy94.trl3lg.data.repository.connection.signup.web.service.ISignUpWebService;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by nikita on 12.06.17.
 */

public class SignUpWebRepository implements ISignUpWebRepository {

    @Inject
    protected ISignUpWebService mSignUpWebService;

    @Inject
    public SignUpWebRepository() {
    }

    @Override
    public Observable<GetFieldsForSignUpResponse> getFieldsForSignUp(GetFieldsForSignUpRequest getFieldsForSignUpRequest) {
        return mSignUpWebService.getFieldsForSignUp(getFieldsForSignUpRequest);
    }

    @Override
    public Observable<SignUpResponse> signUp(SignUpRequest signUpRequest) {
        return mSignUpWebService.signUp(signUpRequest);
    }
}
