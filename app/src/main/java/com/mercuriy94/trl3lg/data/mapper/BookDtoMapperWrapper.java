package com.mercuriy94.trl3lg.data.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.function.Supplier;
import com.mercuriy94.trl3lg.data.Entity.Book;
import com.mercuriy94.trl3lg.data.Entity.dto.BookDto;

import javax.inject.Inject;

/**
 * Created by nikit on 27.08.2017.
 */

public class BookDtoMapperWrapper extends BookDtoMapper<BookDto, Book> {
    @Inject
    public BookDtoMapperWrapper() {
        super(Book::new);
    }
}
