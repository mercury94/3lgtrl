package com.mercuriy94.trl3lg.data.Entity.rest.response;

import com.mercuriy94.trl3lg.data.Entity.Book;
import com.mercuriy94.trl3lg.data.Entity.dto.BookDto;

import java.util.List;

/**
 * Created by nikit on 02.08.2017.
 */

public class FetchBookListResponse extends BaseResponse<List<BookDto>> {
}
