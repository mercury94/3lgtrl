package com.mercuriy94.trl3lg.data.repository.connection.signup.web;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetFieldsForSignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.SignUpRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetFieldsForSignUpResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.SignUpResponse;

import io.reactivex.Observable;

/**
 * Created by nikita on 12.06.17.
 */

public interface ISignUpWebRepository {

    Observable<GetFieldsForSignUpResponse> getFieldsForSignUp(GetFieldsForSignUpRequest getFieldsForSignUpRequest);

    Observable<SignUpResponse> signUp(SignUpRequest signUpRequest);
}
