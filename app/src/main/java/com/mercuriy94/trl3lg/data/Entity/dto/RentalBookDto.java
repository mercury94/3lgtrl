package com.mercuriy94.trl3lg.data.Entity.dto;

import com.google.gson.annotations.SerializedName;
import com.mercuriy94.trl3lg.data.Entity.Book;

/**
 * Created by nikit on 27.08.2017.
 */

public class RentalBookDto extends BookDto {

    @SerializedName("RentalStart")
    private String mRentalStart;
    @SerializedName("RentalEnd")
    private String mRentalEnd;


    public String getRentalStart() {
        return mRentalStart;
    }

    public void setRentalStart(String rentalStart) {
        mRentalStart = rentalStart;
    }

    public String getRentalEnd() {
        return mRentalEnd;
    }

    public void setRentalEnd(String rentalEnd) {
        mRentalEnd = rentalEnd;
    }
}
