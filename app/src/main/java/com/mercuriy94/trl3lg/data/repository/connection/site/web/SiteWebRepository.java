package com.mercuriy94.trl3lg.data.repository.connection.site.web;

import com.mercuriy94.trl3lg.data.Entity.rest.request.GetSitesRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetSitesResponse;
import com.mercuriy94.trl3lg.data.repository.connection.site.service.ISiteWebService;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by nikita on 03.06.17.
 */

public class SiteWebRepository implements ISiteWebRepository {

    @Inject
    protected ISiteWebService mSiteService;

    @Inject
    public SiteWebRepository() {
    }

    @Override
    public Observable<GetSitesResponse> getSites(GetSitesRequest getSitesRequest) {
        return mSiteService.getSites(getSitesRequest);
    }
}
