package com.mercuriy94.trl3lg.data.repository.connection.site.web;


import com.mercuriy94.trl3lg.data.Entity.rest.request.GetSitesRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.GetSitesResponse;

import io.reactivex.Observable;

public interface ISiteWebRepository {

    Observable<GetSitesResponse> getSites(GetSitesRequest getSitesRequest);

}
