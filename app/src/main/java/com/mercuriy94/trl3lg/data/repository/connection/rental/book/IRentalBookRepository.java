package com.mercuriy94.trl3lg.data.repository.connection.rental.book;

import com.mercuriy94.trl3lg.data.Entity.AudioBook;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchBookListRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.request.FetchRentalGroupsRequest;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchBookListResponse;
import com.mercuriy94.trl3lg.data.Entity.rest.response.FetchRentalGroupsResponse;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by nikita on 26.07.17.
 */

public interface IRentalBookRepository {

    Observable<List<FetchRentalGroupsResponse>> getGroups(List<FetchRentalGroupsRequest> request);

    Observable<List<FetchBookListResponse>> fetchBooks(List<FetchBookListRequest> requests);

    Observable<List<AudioBook>> fetchMyAudioBooks();
}
