package com.mercuriy94.trl3lg.presentation.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.function.Supplier;
import com.mercuriy94.trl3lg.data.Entity.Site;
import com.mercuriy94.trl3lg.data.mapper.Mapper;
import com.mercuriy94.trl3lg.presentation.model.SiteModel;

import javax.inject.Inject;

/**
 * Created by nikita on 05.06.17.
 */

public class SiteModelMapper extends Mapper<SiteModel, Site> {

    @Inject
    public SiteModelMapper() {
        super(Site::new);
    }

    @NonNull
    @Override
    protected Site transform(@NonNull SiteModel siteModel, Site site) {
        site.setDomain(siteModel.getDomain());
        site.setUrl(siteModel.getUrl());
        return site;
    }

}
