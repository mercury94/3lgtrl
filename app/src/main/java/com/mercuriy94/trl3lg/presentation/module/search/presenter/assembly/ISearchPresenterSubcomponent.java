package com.mercuriy94.trl3lg.presentation.module.search.presenter.assembly;

import com.mercuriy94.trl3lg.presentation.common.di.presenterbindings.PresenterComponent;
import com.mercuriy94.trl3lg.presentation.common.di.presenterbindings.PresenterComponentBuilder;
import com.mercuriy94.trl3lg.presentation.common.di.scope.PresenterScope;
import com.mercuriy94.trl3lg.presentation.module.auth.presenter.assembly.AuthPresenterModule;
import com.mercuriy94.trl3lg.presentation.module.auth.presenter.assembly.IAuthPresenterSubcomponent;
import com.mercuriy94.trl3lg.presentation.module.search.presenter.SearchPresenter;

import dagger.Subcomponent;

/**
 * Created by nikit on 02.08.2017.
 */
@PresenterScope
@Subcomponent(modules = SearchPresenterModule.class)
public interface ISearchPresenterSubcomponent extends PresenterComponent<SearchPresenter> {

    @Subcomponent.Builder
    interface Builder extends PresenterComponentBuilder<SearchPresenterModule, ISearchPresenterSubcomponent> {

    }

}
