package com.mercuriy94.trl3lg.presentation.common.di.datastore;


import com.mercuriy94.trl3lg.presentation.common.di.repository.RepositoryModule;

import dagger.Module;


@Module(includes = RepositoryModule.class)
public  abstract class DataStoreModule {

}
