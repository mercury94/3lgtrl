package com.mercuriy94.trl3lg.presentation.module.main.submodule.ondemand.view.drugandswipe;

import android.support.v7.widget.RecyclerView;

/**
 * Created by nikita on 23.08.17.
 */

public interface OnStartDrugListener {

    void onStartDrag(RecyclerView.ViewHolder viewHolder);

}
