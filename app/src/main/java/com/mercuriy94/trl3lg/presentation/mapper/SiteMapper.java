package com.mercuriy94.trl3lg.presentation.mapper;

import android.support.annotation.NonNull;

import com.annimon.stream.function.Supplier;
import com.mercuriy94.trl3lg.data.Entity.Site;
import com.mercuriy94.trl3lg.data.mapper.Mapper;
import com.mercuriy94.trl3lg.presentation.model.SiteModel;

import javax.inject.Inject;

/**
 * Created by nikita on 03.06.17.
 */

public class SiteMapper extends Mapper<Site, SiteModel> {

    @Inject
    public SiteMapper() {
        super(SiteModel::new);
    }

    @NonNull
    @Override
    protected SiteModel transform(@NonNull Site site, SiteModel siteModel) {
        siteModel.setDomain(site.getDomain());
        siteModel.setUrl(site.getUrl());
        return siteModel;
    }
}
