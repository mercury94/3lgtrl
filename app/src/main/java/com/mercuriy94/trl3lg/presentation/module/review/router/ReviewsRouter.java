package com.mercuriy94.trl3lg.presentation.module.review.router;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.mercuriy94.trl3lg.presentation.module.review.ReviewsModuleContract;

/**
 * Created by nikita on 22.10.17.
 */

public class ReviewsRouter extends ReviewsModuleContract.AbstractReviewsRouter {
    @Override
    public BaseRouterAdapter createAdapter(@NonNull AppCompatActivity appCompatActivity) {
        return new BaseRouterAdapter(appCompatActivity);
    }
}
