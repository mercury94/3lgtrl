package com.mercuriy94.trl3lg.presentation.module.main.presenter.assembly;

import com.mercuriy94.trl3lg.presentation.common.di.presenterbindings.PresenterModule;
import com.mercuriy94.trl3lg.presentation.module.auth.presenter.AuthPresenter;
import com.mercuriy94.trl3lg.presentation.module.main.presenter.MainPresenter;

import dagger.Module;

/**
 * Created by nikita on 22.07.17.
 */

@Module
public class MainPresenterModule extends PresenterModule<MainPresenter> {
    public MainPresenterModule(MainPresenter mainPresenter) {
        super(mainPresenter);
    }
}
