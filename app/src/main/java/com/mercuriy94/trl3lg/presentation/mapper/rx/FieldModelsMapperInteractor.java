package com.mercuriy94.trl3lg.presentation.mapper.rx;

import android.support.annotation.NonNull;

import com.mercuriy94.trl3lg.data.Entity.rest.response.GetFieldsForSignUpResponse;
import com.mercuriy94.trl3lg.domain.common.BaseInteractor;
import com.mercuriy94.trl3lg.presentation.common.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.trl3lg.presentation.mapper.FieldModelMapper;
import com.mercuriy94.trl3lg.presentation.model.FieldModel;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by nikita on 24.06.17.
 */

public class FieldModelsMapperInteractor extends BaseInteractor<List<FieldModel>, List<GetFieldsForSignUpResponse.DataMemberField>> {

    @Inject
    FieldModelMapper mFieldModelMapper;

    @Inject
    public FieldModelsMapperInteractor(
            @Named(RxSchedulerModule.COMPUTATION) @NonNull Scheduler subscriberScheduler,
            @Named(RxSchedulerModule.MAIN) @NonNull Scheduler observerScheduler) {
        super(subscriberScheduler, observerScheduler);
    }

    @Override
    protected Observable<List<FieldModel>> buildObservable(List<GetFieldsForSignUpResponse.DataMemberField> dataMemberFields) {
        return Observable.fromCallable(() -> mFieldModelMapper.execute(dataMemberFields));
    }
}
