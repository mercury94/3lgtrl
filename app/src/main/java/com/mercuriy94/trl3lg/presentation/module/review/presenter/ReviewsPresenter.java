package com.mercuriy94.trl3lg.presentation.module.review.presenter;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.InjectViewState;
import com.mercuriy94.trl3lg.domain.review.FetchReviewsInteractor;
import com.mercuriy94.trl3lg.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.trl3lg.presentation.model.TitleModel;
import com.mercuriy94.trl3lg.presentation.module.review.ReviewsModuleContract;
import com.mercuriy94.trl3lg.presentation.module.review.presenter.assembly.IReviewsPresenterSubmodule;
import com.mercuriy94.trl3lg.presentation.module.review.presenter.assembly.ReviewsPresenterModule;

import javax.inject.Inject;

/**
 * Created by nikita on 22.10.17.
 */

@InjectViewState
public class ReviewsPresenter extends ReviewsModuleContract.AbstractReviewsPresenter {

    @Inject
    FetchReviewsInteractor mFetchReviewsInteractor;

    public ReviewsPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        super(presenterSubcomponentBuilders);
    }

    @Override
    public void inject(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
        ((IReviewsPresenterSubmodule.Builder) presenterSubcomponentBuilders.getPresenterComponentBuilder(ReviewsPresenter.class))
                .presenterModule(new ReviewsPresenterModule(this))
                .build()
                .injectMembers(this);
    }

    @Override
    protected TitleModel getTitle() {
        return new TitleModel.Builder()
                .setTitleMessage("Reviews")
                .setVisibleBackButton(true)
                .build();
    }
}
