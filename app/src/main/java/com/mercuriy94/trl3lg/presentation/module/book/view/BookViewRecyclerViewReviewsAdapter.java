package com.mercuriy94.trl3lg.presentation.module.book.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mercuriy94.trl3lg.R;
import com.mercuriy94.trl3lg.data.Entity.Review;
import com.mercuriy94.trl3lg.presentation.common.view.BaseRecyclerViewAdapter;
import com.mercuriy94.trl3lg.presentation.utils.view.list.viewholder.ReviewViewHolder;

/**
 * Created by nikita on 20.10.17.
 */

public class BookViewRecyclerViewReviewsAdapter extends BaseRecyclerViewAdapter<ReviewViewHolder, Review> {

    public static final String TAG = "BookViewRecyclerViewReviewsAdapter";
    private final int TYPE_ITEM = 0;
    private final int TYPE_FOOTER = 1;
    private final int mCountFooters = 1;
    private boolean mVisiblePendingFooter = true;

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_review_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        holder.bind(getItemList().get(position));
    }

}
