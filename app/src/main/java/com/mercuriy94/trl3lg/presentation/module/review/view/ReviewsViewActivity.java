package com.mercuriy94.trl3lg.presentation.module.review.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.mercuriy94.trl3lg.R;
import com.mercuriy94.trl3lg.data.Entity.Review;
import com.mercuriy94.trl3lg.presentation.common.annotations.Layout;
import com.mercuriy94.trl3lg.presentation.common.view.BaseListActivity;
import com.mercuriy94.trl3lg.presentation.module.app.App;
import com.mercuriy94.trl3lg.presentation.module.review.ReviewsModuleContract;
import com.mercuriy94.trl3lg.presentation.module.review.presenter.ReviewsPresenter;
import com.mercuriy94.trl3lg.presentation.module.review.router.ReviewsRouter;

import butterknife.BindView;

/**
 * Created by nikita on 22.10.17.
 */

@Layout(R.layout.act_reviews_layout)
public class ReviewsViewActivity extends BaseListActivity<ReviewsModuleContract.AbstractReviewsPresenter,
        ReviewsModuleContract.AbstractReviewsRouter,
        Review,
        ReviewsModuleRecyclerAdapter,
        LinearLayoutManager>
        implements ReviewsModuleContract.IReviewsView {

    public static final String EXTRA_BOOK_TITLE = "book_title";
    public static final String EXTRA_BOOK_ID = "book_id";

    @InjectPresenter
    ReviewsModuleContract.AbstractReviewsPresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static Intent newIntent(Context context, String bookTitle, String bookId) {
        Intent intent = new Intent(context, ReviewsViewActivity.class);
        intent.putExtra(EXTRA_BOOK_ID, bookId);
        intent.putExtra(EXTRA_BOOK_TITLE, bookTitle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(mToolbar);
    }

    @ProvidePresenter
    ReviewsModuleContract.AbstractReviewsPresenter providePresenter() {
        return new ReviewsPresenter(App.getHasPresenterSubcomponentBuilders(this));
    }

    @Override
    protected ReviewsModuleContract.AbstractReviewsRouter resolveRouter() {
        return new ReviewsRouter();
    }

    @NonNull
    @Override
    public ReviewsModuleContract.AbstractReviewsPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onRefreshLayout() {

    }

    @NonNull
    @Override
    protected ReviewsModuleRecyclerAdapter createAdapter() {
        return new ReviewsModuleRecyclerAdapter();
    }

    @NonNull
    @Override
    protected LinearLayoutManager createLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    @Override
    public void setVisibilityBackButton(boolean value) {
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(value);
    }

    @Override
    public void setTitleText(String title) {
        if (getSupportActionBar() != null) getSupportActionBar().setTitle(title);
    }

}
