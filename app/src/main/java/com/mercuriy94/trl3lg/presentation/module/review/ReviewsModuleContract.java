package com.mercuriy94.trl3lg.presentation.module.review;

import android.support.annotation.NonNull;

import com.mercuriy94.trl3lg.presentation.common.di.presenterbindings.HasPresenterSubcomponentBuilders;
import com.mercuriy94.trl3lg.presentation.common.presenter.ViperBasePresenter;
import com.mercuriy94.trl3lg.presentation.common.router.BaseRouter;
import com.mercuriy94.trl3lg.presentation.common.view.IBaseView;

/**
 * Created by nikita on 22.10.17.
 */

public abstract class ReviewsModuleContract {

    private ReviewsModuleContract() {
        throw new RuntimeException(" no instance please!");
    }


    public interface IReviewsView extends IBaseView {

    }

    public abstract static class AbstractReviewsPresenter extends ViperBasePresenter<IReviewsView, AbstractReviewsRouter> {

        public AbstractReviewsPresenter(@NonNull HasPresenterSubcomponentBuilders presenterSubcomponentBuilders) {
            super(presenterSubcomponentBuilders);
        }
    }


    public static abstract class AbstractReviewsRouter extends BaseRouter<BaseRouter.BaseRouterAdapter> {

    }

}


