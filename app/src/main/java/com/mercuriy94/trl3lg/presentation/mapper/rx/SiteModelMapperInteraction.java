package com.mercuriy94.trl3lg.presentation.mapper.rx;

import android.support.annotation.NonNull;

import com.mercuriy94.trl3lg.data.Entity.Site;
import com.mercuriy94.trl3lg.domain.common.BaseInteractor;
import com.mercuriy94.trl3lg.presentation.common.di.rxschedulers.RxSchedulerModule;
import com.mercuriy94.trl3lg.presentation.mapper.SiteModelMapper;
import com.mercuriy94.trl3lg.presentation.model.SiteModel;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by nikita on 05.06.17.
 */

public class SiteModelMapperInteraction extends BaseInteractor<Site, SiteModel> {

    @Inject
    protected SiteModelMapper mSiteModelMapper;

    @Inject
    public SiteModelMapperInteraction(
            @Named(RxSchedulerModule.COMPUTATION) @NonNull Scheduler subscriberScheduler,
            @Named(RxSchedulerModule.MAIN) @NonNull Scheduler observerScheduler) {
        super(subscriberScheduler, observerScheduler);
    }

    @Override
    protected Observable<Site> buildObservable(SiteModel siteModel) {
        return Observable.fromCallable(() -> mSiteModelMapper.execute(siteModel));
    }

}
