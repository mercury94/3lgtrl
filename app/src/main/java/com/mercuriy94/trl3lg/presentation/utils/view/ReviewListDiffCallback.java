package com.mercuriy94.trl3lg.presentation.utils.view;

import android.support.v7.util.DiffUtil;

import com.mercuriy94.trl3lg.data.Entity.Review;

import java.util.List;

/**
 * Created by nikita on 22.10.17.
 */

public class ReviewListDiffCallback extends DiffUtil.Callback {

    private List<Review> mOldReviewList;
    private List<Review> mNewReviewList;
    private int mCountFooters;

    public ReviewListDiffCallback(List<Review> oldReviewList, List<Review> newReviewList, int countFooter) {
        mOldReviewList = oldReviewList;
        mNewReviewList = newReviewList;
        mCountFooters = countFooter;
    }

    @Override
    public int getOldListSize() {
        return mOldReviewList.size() + mCountFooters;
    }

    @Override
    public int getNewListSize() {
        return mNewReviewList.size() + mCountFooters;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        if (isFooter(oldItemPosition, newItemPosition)) return false;
        return mOldReviewList.get(oldItemPosition).getId().equals(mNewReviewList.get(newItemPosition).getId());
    }

    private boolean isFooter(int oldItemPosition, int newItemPosition) {
        return (oldItemPosition >= mOldReviewList.size()) |
                (newItemPosition >= mNewReviewList.size());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldReviewList.get(oldItemPosition).equals(mNewReviewList.get(newItemPosition));
    }
}
