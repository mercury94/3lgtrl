package com.mercuriy94.trl3lg.presentation.common.view;

/**
 * Created by nikita on 15.03.2017.
 */

public interface IOnBackPressedListener {

    boolean onBackPressed();
}
